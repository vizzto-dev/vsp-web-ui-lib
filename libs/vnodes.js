export default {

    cloneNode: function (h, vnode, suffix) {
        const isText = !vnode.tag;
        if(isText) {
            return vnode.text;
        } else {
            const clonedData = {...vnode.data};
            clonedData.attrs = {...vnode.data.attrs};
            clonedData.attrs['data-copy'] = true;
            const clonedChildren = vnode.children && vnode.children.map(vnode => this.cloneNode(h, vnode));
            const cloned = h(vnode.tag, clonedData, clonedChildren);
            cloned.text = vnode.text;
            cloned.isComment = vnode.isComment;
            cloned.componentOptions = vnode.componentOptions;
            cloned.elm = vnode.elm;
            cloned.context = vnode.context;
            cloned.ns = vnode.ns;
            cloned.isStatic = vnode.isStatic;
            cloned.key = vnode.key + '-' + suffix;
            return cloned;
        }
    },

    cloneNodes: function (h, nodesList, suffix) {
        const clones = [];
        for(let a = 0, n = nodesList.length; a < n; a++) {
            clones.push(this.cloneNode(h, nodesList[a], suffix + '-' + a))
        }
        return clones;
    }

}