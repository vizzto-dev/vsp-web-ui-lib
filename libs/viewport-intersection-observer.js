class ViewObserver {

    constructor(viewportMargin) {
        this.entries = [];
        this.observer = this.createObserver(viewportMargin || 128);
    }

    observe(element, callback) {
        const exists = this.entries.find(entry => entry.element === element);
        if(exists) {
            exists.callback = callback;
        } else {
            this.entries.push({ element, callback });
            this.observer.observe(element);
        }
    }

    unobserveAt(index) {
        const entry = this.entries[index];
        this.observer.unobserve(entry.element);
        this.entries.splice(index, 1);
    }

    unobserve(element) {
        const entryIndex = this.entries.findIndex(entry => entry.element === element);
        if(entryIndex >= 0) {
            this.unobserveAt(entryIndex);
        }
    }

    handleEntryIntersection({ intersectionRatio, target}) {
        if(intersectionRatio > 0) {
            const entryIndex = this.entries.findIndex(item => item.element === target);
            if(entryIndex >= 0) {
                this.entries[entryIndex].callback();
                this.unobserveAt(entryIndex);
            }
        }
    }

    createObserver(rootMargin) {
        let observer;
        if(typeof IntersectionObserver !== 'undefined') {
            observer = new IntersectionObserver(entries => {
                entries.forEach(this.handleEntryIntersection, this);
            }, {
                rootMargin: rootMargin + 'px',
                threshold: 0.1
            });
        } else {
            const nothing = function () {};
            observer = {
                observe: nothing,
                unobserve: nothing
            };
        }
        return observer;
    }

}

let instance = null;

function getInstance() {
    if(!instance) {
        instance = new ViewObserver();
    }
    return instance;
}

export default {

    observe: function (element, callback) {
        getInstance().observe(element, callback);
    },

    unobserve: function (element) {
        getInstance().unobserve(element);
    }

}