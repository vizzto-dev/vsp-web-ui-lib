/**
 * Adds scrolling and pagination to html element.
 * It doesn't provide any css besides inline _translate_ to move elements.
 * You should set the css right (i.e. with `overflow: hidden` and animations).
 * @author Lanza
 */
export class Scrollanza {

  /**
   * Scrollanza options.
   * @typedef {Object} ConstructorOptions
   * @property {{elementNext: HTMLElement|null , elementPrevious: HTMLElement|null, enabledClass: String}} navigation Options for next, previous buttons.
   * @property {{containerElement: HTMLElement|null, activeClass: String}} pagination Options to pagination links. Scrollanza creates _span_ tags for each page.
   */

  /**
   * Creates the scroller.
   * @param {HTMLElement} track Element that will be moved and has the slides within.
   * @param {HTMLElement|Window} [container] Element that contains the track and set the visible canvas. Default is the document itself.
   * @param {ConstructorOptions} options Scrollanza options.
   * @param {Document} document Document that contains the track.
   * @param {Window} window Window that contains the document.
   */
  constructor(track, container, options, document, window) {
    if(track) {
      const opts = {
        navigation: {
          elementNext: null,
          elementPrevious: null,
          enabledClass: 'enabled'
        },
        pagination: {
          containerElement: null,
          activeClass: 'active'
        },
        ...options
      };
      this.verticalMode = opts.vertical || false;
      this.track = {
        element: track,
        size: 0
      };
      this.container = {
        element: container || window,
        size: 0
      };
      this.pages = {
        element: opts.pagination.containerElement,
        current: -1,
        coords: [0],
        activeClass: opts.activeClass
      };
      this.navigation = {
        next: {
          enabled: false,
          element: opts.navigation.elementNext,
          fn: null,
        },
        prev: {
          enabled: false,
          element: opts.navigation.elementPrevious,
          fn: null
        },
        enabledClass: opts.navigation.enabledClass,
        swipe: {
          fnStart: null,
          fnEnd: null
        }
      };
      this.viewport = {
        element: window,
        fn: null
      };
      this.listeners = [];
      this.create();
    }
  }

  updatePagesInterface() {
    const el = this.pages.element;
    const nPages = this.pages.coords.length;
    const activeClass = this.pages.activeClass || 'active';
    if(el) {
      if(el.childElementCount !== nPages) {
        el.innerHTML = '';
        if(nPages > 1) {
          for(let a = 0; a < nPages; a++) {
            let span = document.createElement('span');
            el.appendChild(span);
            span.addEventListener('click', this.gotoPage.bind(this, a));
          }
        }
      }
      let pageEl = el.querySelector('span.' + activeClass);
      if(pageEl) {
        pageEl.classList.remove(activeClass);
      }
      pageEl = el.querySelector('span:nth-child(' + (this.pages.current + 1) + ')');
      if(pageEl) {
        pageEl.classList.add(activeClass);
      }
    }
  }

  updateNavigationInterface() {
    const next = this.navigation.next;
    const prev = this.navigation.prev;
    const btActiveClass = this.navigation.enabledClass;
    if(next.element) {
      next.element.classList.toggle(btActiveClass, next.enabled);
    }
    if(prev.element) {
      prev.element.classList.toggle(btActiveClass, prev.enabled);
    }
  }

  updateTrackInterface() {
    const coord = this.pages.coords[this.pages.current];
    this.track.element.style.transform = this.verticalMode ? `translate(0, -${coord}px)` : `translate(-${coord}px, 0)`;
  }

  updateAllInterface() {
    this.updateNavigationInterface();
    this.updatePagesInterface();
    this.updateTrackInterface();
  }

  getContainerSize() {
    let elDim, viewDim;
    const container = this.container.element;
    if(this.verticalMode) {
      elDim = 'height';
      viewDim = 'innerHeight';
    } else {
      elDim = 'width';
      viewDim = 'innerWidth';
    }
    if(container.getBoundingClientRect) {
      const rect = container.getBoundingClientRect();
      return rect[elDim];
    } else if(container.hasOwnProperty(viewDim)) {
      return container[viewDim];
    } else {
      return 0;
    }
  }

  defineNavigation() {
    const p = this.pages;
    this.navigation.prev.enabled = p.current > 0;
    this.navigation.next.enabled = p.current < p.coords.length - 1;
  }

  definePagination() {
    const trackRect = this.track.element.getBoundingClientRect();
    let dimension, posProp;
    if(this.verticalMode) {
      dimension = 'height';
      posProp = 'offsetTop';
    } else {
      dimension = 'width';
      posProp = 'offsetLeft';
    }
    const trackSize = trackRect[dimension] + this.track.element[posProp];
    const areaSize = this.getContainerSize();
    const sizesDiff = trackSize - areaSize;
    this.track.size = trackSize;
    this.container.size = areaSize;
    this.pages.coords = [0];
    if(sizesDiff > 0 && this.track.element.childElementCount) {
      let elementsSize = 0;
      let prevElRect = null;
      let prevEl = null;
      let prevCoord = 0;
      for(let a = 0, n = this.track.element.childElementCount; a < n; a++) {
        let el = this.track.element.children[a];
        let elRect = el.getBoundingClientRect();
        elementsSize += elRect[dimension];
        if(prevElRect) {
          elementsSize += el[posProp] - prevEl[posProp] - prevElRect[dimension];
        }
        if(elementsSize > areaSize) {
          let newCoord = Math.min(prevCoord + elementsSize - elRect[dimension], sizesDiff);
          this.pages.coords.push(newCoord);
          elementsSize = elRect[dimension];
          prevCoord = newCoord;
        }
        prevElRect = elRect;
        prevEl = el;
      }
    }
  }

  dispatchNavigationEvents() {
    if(!this.navigation.next.enabled) {
      this.dispatchEvent('reachedEnd');
    }
  }

  gotoPage(pageIndex) {
    if(pageIndex !== this.pages.current) {
      this.pages.current = pageIndex;
      this.defineNavigation();
      this.updateAllInterface();
      this.dispatchNavigationEvents();
    }
  }

  nextPage() {
    if(this.navigation.next.enabled) {
      this.gotoPage(Math.min(this.pages.current + 1, this.pages.coords.length - 1));
    }
  }

  prevPage() {
    if(this.navigation.prev.enabled) {
      this.gotoPage(Math.max(this.pages.current - 1, 0));
    }
  }

  gotoCloserPage() {
    const dim = this.verticalMode ? 'top' : 'left';
    const trackRect = this.track.element.getBoundingClientRect();
    const containerRect = this.container.element.getBoundingClientRect();
    const trackPos = trackRect[dim] - containerRect[dim];
    const pagesCoords = this.pages.coords;
    let distance = Number.MAX_SAFE_INTEGER;
    let coordIndex = 0;
    for(let a = 0, n = pagesCoords.length; a < n; a++) {
      let coordDistance = Math.abs(trackPos + pagesCoords[a]);
      if(coordDistance < distance) {
        distance = coordDistance;
        coordIndex = a;
      }
    }
    this.gotoPage(coordIndex);
  }

  enableSwipe() {
    let pstart = 0, pend = 0;
    const eventProp = this.verticalMode ? 'screenY' : 'screenX';
    const container = this.container.element;
    const swipe = this.navigation.swipe;

    swipe.fnStart = evt => {
      pstart = evt.changedTouches[0][eventProp];
    };

    swipe.fnEnd = evt => {
      pend = evt.changedTouches[0][eventProp];
      const diff = pend - pstart;
      if(diff > 10) {
        this.prevPage();
      } else if(diff < -10) {
        this.nextPage();
      }
    };

    container.addEventListener('touchstart', swipe.fnStart);
    container.addEventListener('touchend', swipe.fnEnd);
  }

  disableSwipe() {
    const container = this.container.element;
    const swipe = this.navigation.swipe;
    if(swipe.fnStart) {
      container.removeEventListener('touchstart', swipe.fnStart);
      swipe.fnStart = null;
    }
    if(swipe.fnEnd) {
      container.removeEventListener('touchend', swipe.fnEnd);
      swipe.fnEnd = null;
    }
  }

  enableNavigation() {
    const next = this.navigation.next;
    const prev = this.navigation.prev;
    if(next.element) {
      next.fn = this.nextPage.bind(this);
      next.element.addEventListener('click', next.fn);
    }
    if(prev.element) {
      prev.fn = this.prevPage.bind(this);
      prev.element.addEventListener('click', prev.fn);
    }
  }

  disableNavigation() {
    const next = this.navigation.next;
    const prev = this.navigation.prev;
    if(next.fn) {
      next.element.removeEventListener('click', next.fn);
      next.fn = null;
    }
    if(prev.fn) {
      prev.element.removeEventListener('click', prev.fn);
      prev.fn = null;
    }
  }

  disable() {
    this.disableNavigation();
    this.disableSwipe();
  }

  resize() {
    const prevCoords = this.pages.coords;
    this.definePagination();
    const changed = this.pages.coords.length !== prevCoords.length ||
      this.pages.coords.some((coord, index) => {
        return coord !== prevCoords[index];
      });
    if(changed) {
      this.gotoCloserPage();
    }
  }

  listenToResize() {
    let frame = null;
    const view = this.viewport;
    const fn = () => {
      cancelAnimationFrame(frame);
      frame = requestAnimationFrame(this.resize.bind(this));
    };
    view.element.addEventListener('resize', fn);
    view.fn = fn;
  }

  unlistenToResize() {
    const view = this.viewport;
    if(view.fn) {
      view.element.removeEventListener('resize', view.fn);
      view.fn = null;
    }
  }

  addEventListener(eventName, callback, context) {
    this.listeners.push({
      event: eventName,
      fn: callback,
      context: context || null
    });
  }

  removeEventListener(eventName, callback) {
    this.listeners = this.listeners.filter(listener => !(listener.event === eventName && listener.fn === callback));
  }

  removeAllEventListeners() {
    this.listeners = [];
  }

  dispatchEvent(eventName) {
    const matches = this.listeners.filter(listener => listener.event === eventName);
    for(let a = 0, n = matches.length; a < n; a++) {
      let listener = matches[a];
      listener.fn.call(listener.context);
    }
  }

  /**
   * Destroys Scrollanza. Removes all event listeners.
   */
  destroy() {
    this.disable();
    this.unlistenToResize();
    this.removeAllEventListeners();
  }

  create() {
    this.definePagination();
    this.enableNavigation();
    this.enableSwipe();
    this.gotoCloserPage();
    this.listenToResize();
  }

}
