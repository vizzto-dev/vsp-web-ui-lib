# Vizzto Components Library

Library with Vue components to use in Vizzto or other Vue projects.
The components are organized in atoms, molecules and organisms, following the principles of [Atomic Design](https://bradfrost.com/blog/post/atomic-web-design/).
