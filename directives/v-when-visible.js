import ViewIntersectionObserver from '../libs/viewport-intersection-observer';

export default {

    inserted: function (el, binding) {
        ViewIntersectionObserver.observe(el, binding.value);
    },

    componentUpdated: function (el, binding) {
        //ViewIntersectionObserver.observe(el, binding.value);
    },

    unbind: function (el) {
        ViewIntersectionObserver.unobserve(el);
    }

}