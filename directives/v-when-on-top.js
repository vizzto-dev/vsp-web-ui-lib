/**
 * Watches page's vertical scroll and triggers callback function when an element reaches the top.
 * @author Lanza
 */
class ScrollObserver {

    /**
     * Constructor.
     * @param {HTMLElement} element
     * @param {Number} enterTop When element reach this position, trigger callback function.
     * @param {Number} leaveTop When the bottom of element reach this position, trigger callback function.
     * @param {Function} enterCallback Callback function to be triggered when element enters in range.
     * @param {Function} leaveCallback Callback function to be triggered when element leaves range.
     * @param {Window} viewport Viewport object, usually Window.
     * @author Lanza
     */
    constructor(element, enterTop, leaveTop, enterCallback, leaveCallback, viewport) {
        this.element = element;
        this.enterTop = enterTop;
        this.leaveTop = leaveTop;
        this.enterCallback = enterCallback;
        this.leaveCallback = leaveCallback;
        this.viewport = viewport;
        this.inRange = false;
        this.animationFrame = null;
        this.scrollHandler = this.watchScroll.bind(this);

        this.scrollHandler();
        this.viewport.addEventListener('scroll', this.scrollHandler);
    }

    checkPosition() {
        const oldInRange = this.inRange;
        const elRect = this.element.getBoundingClientRect();
        this.inRange = elRect.top <= this.enterTop && elRect.bottom > this.leaveTop;
        if(this.inRange !== oldInRange) {
            if(this.inRange) {
                this.enterCallback();
            } else {
                this.leaveCallback();
            }
        }
    }

    watchScroll() {
        this.viewport.cancelAnimationFrame(this.animationFrame);
        this.animationFrame = this.viewport.requestAnimationFrame(this.checkPosition.bind(this));
    }

    /**
     * Destructor.
     * @author Lanza
     */
    destroy() {
        this.viewport.removeEventListener('scroll', this.scrollHandler);
    }

}

export default {

    /**
     * When element is inserted in component.
     * @param {HTMLElement} el
     * @param {{ value: { onEnter:Function, onLeave:Function, marginTop:Number, marginBottom:Number} }} binding
     */
    inserted: function (el, binding) {
        const params = binding.value;
        const marginTop = params.marginTop || 0;
        el._when_on_top_obj = new ScrollObserver(el, marginTop, marginTop - (params.marginBottom || 0), params.onEnter, params.onLeave, window);
    },

    /**
     * When element will be removed from component.
     * @param {HTMLElement} el
     * @param {{ value: { callback:Function, screenTop:Number, marginBottom:Number} }} binding
     */
    unbind: function (el, binding) {
        if(el._when_on_top_obj) {
            el._when_on_top_obj.destroy();
        }
    }

}