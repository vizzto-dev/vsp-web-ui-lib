const TRACKSELECTOR = '.vz-carousel-track';
const PAGESELECTOR = '.vz-carousel-track-page';
const ORIGINALPAGESELECTOR = '.vz-carousel-track-page-original';
const NEXTPAGESELECTOR = '.vz-carousel-next';
const PREVPAGESELECTOR = '.vz-carousel-prev';

class CarouselHelper {

    constructor(container, binding, viewport) {
        this.container = container;
        this.binding = binding;
        this.rect = null;
        this.resizeFn = null;
        this.prevPageFn = null;
        this.nextPageFn = null;
        this.viewport = viewport;
        this.listeners = [];
        this.create();
    }

    getOriginalElementsSize() {
        let res = 0;
        const els = this.container.querySelector(ORIGINALPAGESELECTOR).children;
        for(let a = 0, n = els.length; a < n; a++) {
            let child = els[a];
            if(!child.getAttribute('data-copy')) {
                res += child.getBoundingClientRect().width;
            }
        }
        return res;
    }

    executeResizeCallback() {
        const container = this.container;
        const slotsWidth = this.getOriginalElementsSize();
        this.rect = container.getBoundingClientRect();
        this.binding.value.resize.call(null, this.rect.width, slotsWidth);
    }

    getTrackElements() {
        const res = [];
        this.container.querySelectorAll(PAGESELECTOR).forEach(page => {
            res.push(...page.children);
        });
        return res;
    }

    getOriginalElement(elementIndex) {
        return this.container.querySelector(ORIGINALPAGESELECTOR).children[elementIndex];
    }

    getNextElement() {
        const children = this.getTrackElements();
        const containerRight = this.rect.right;
        let minDiff = this.rect.width;
        let element = null;
        for(let a = 0, n = children.length; a < n; a++) {
            let el = children[a];
            let diff = el.getBoundingClientRect().right - containerRight;
            if(diff > 0 && diff < minDiff) {
                minDiff = diff;
                element = el;
            }
        }
        return element;
    }

    getPrevElement() {
        const children = this.getTrackElements();
        const containerLeft = this.rect.left;
        let minDiff = -this.rect.width;
        let element = null;
        for(let a = 0, n = children.length; a < n; a++) {
            let el = children[a];
            let diff = el.getBoundingClientRect().left - containerLeft;
            if(diff < 0 && diff > minDiff) {
                minDiff = diff;
                element = el;
            }
        }
        return element;
    }

    getElementOffset(element, parent) {
        let el = element;
        let res = 0;
        do {
            res += el.offsetLeft;
            el = el.parentNode;
        } while(el && el !== parent);
        return res;
    }

    getElementPositionsInCarousel(elementData) {
        return {
            position: this.getElementOffset(elementData.element, this.container),
            originalPosition: this.getElementOffset(this.getOriginalElement(elementData.index), this.container)
        };
    }

    dispatchEvent(eventName) {
        const matches = this.listeners.filter(listener => listener.event === eventName);
        for(let a = 0, n = matches.length; a < n; a++) {
            let listener = this.listeners[a];
            listener.fn.call(listener.context);
        }
    }

    nextPage() {
        const element = this.getNextElement();
        if(element) {
            const position = this.getElementOffset(element, this.container);
            const pageSize = this.container.querySelector(PAGESELECTOR).getBoundingClientRect().width;
            const diff = position - pageSize;
            let reposition = position;
            if(diff > 0) {
                reposition = diff;
                this.dispatchEvent('reachedEnd');
            }
            this.binding.value.position.call(null, 0 - position, 0 - reposition, false);
        }
    }

    prevPage() {
        const element = this.getPrevElement();
        if(element) {
            const appendWidth = this.rect.width - element.offsetWidth;
            const position = this.getElementOffset(element, this.container) - appendWidth;
            const pageSize = this.container.querySelector(PAGESELECTOR).getBoundingClientRect().width;
            const reposition = position <= 0 ? position + pageSize : position;
            this.binding.value.position.call(null, 0 - position, 0 - reposition, true);
        }
    }

    listenResize() {
        let frame = null;
        const resizeTrigger = () => {
            this.viewport.cancelAnimationFrame(frame);
            frame = this.viewport.requestAnimationFrame(this.executeResizeCallback.bind(this));
        };
        this.viewport.addEventListener('resize', resizeTrigger);
        this.resizeFn = resizeTrigger;
    }

    unlistenResize() {
        if(this.resizeFn) {
            this.viewport.removeEventListener('resize', this.resizeFn);
            this.resizeFn = null;
        }
    }

    listenButtons() {
        const container = this.container;
        const prevPageFn = this.prevPage.bind(this);
        const nextPageFn = this.nextPage.bind(this);
        const btPrev = container.querySelector(PREVPAGESELECTOR);
        if(btPrev) {
            btPrev.addEventListener('click', prevPageFn);
            this.prevPageFn = prevPageFn;
        }
        const btNext = container.querySelector(NEXTPAGESELECTOR);
        if(btNext) {
            btNext.addEventListener('click', nextPageFn);
            this.nextPageFn = nextPageFn;
        }
    }

    unlistenButtons() {
        const container = this.container;
        if(this.prevPageFn) {
            container.querySelector(PREVPAGESELECTOR).removeEventListener('click', this.prevPageFn);
            this.prevPageFn = null;
        }
        if(this.nextPageFn) {
            container.querySelector(NEXTPAGESELECTOR).removeEventListener('click', this.nextPageFn);
            this.nextPageFn = null;
        }
    }

    addEventListener(eventName, callback, context) {
        this.listeners.push({
            event: eventName,
            fn: callback,
            context: context || null
        });
    }

    removeEventListener(eventName, callback) {
        this.listeners = this.listeners.filter(listener => !(listener.event === eventName && listener.fn === callback));
    }

    removeAllEventListeners() {
        this.listeners = [];
    }

    destroy() {
        this.unlistenResize();
        this.unlistenButtons();
        this.removeAllEventListeners();
    }

    create() {
        this.listenResize();
        this.listenButtons();
        this.resizeFn();
    }
}

export default {

    inserted: function (el, binding) {
        /*
            binding.value
                         .resize (containerWidth, pageWidth)
                         .position (transitionPosition, endTransitionPosition, isPrevPage)
                         .onEnd ()
         */
        const helper = new CarouselHelper(el, binding, window);
        helper.addEventListener('reachedEnd', binding.value.onEnd);
        el._carousel_helper = helper;

    },

    unbind: function (el) {
        if(el._carousel_helper) {
            el._carousel_helper.destroy();
            el._carousel_helper = null;
        }
    }

}